export const Config = {
	api: {
		port: import.meta.env.API_PORT ?? 3000,
	},
	redis: {
		url: import.meta.env.REDIS_URL!,
	},
	crawler: {
		userAgent: import.meta.env.CRAWLER_USER_AGENT!,
	},
} as const
