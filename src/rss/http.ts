import type { RenderRssProps } from './render'
import { Hono } from 'hono'
import { etag } from 'hono/etag'
import { rssRender } from './render-middleware'
import FacebookPage from './sites/facebook/controllers/page'
import RutenStore from './sites/ruten/controllers/store'

export interface Env {
	Variables: {
		rss: RenderRssProps | undefined
	}
}

export default new Hono<Env>()
	.use(rssRender)
	.use(etag())
	.route('/', RutenStore)
	.route('/', FacebookPage)
