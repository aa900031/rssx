import type { Env } from './http'
import { createMiddleware } from 'hono/factory'
import { renderRss } from './render'

export const rssRender = createMiddleware<Env>(async (ctx, next) => {
	await next()

	const rss = ctx.get('rss')
	if (!rss)
		return

	const body = await renderRss(rss)
	ctx.header('Content-Type', 'application/xml;charset=utf-8')
	ctx.header('Access-Control-Allow-Methods', 'GET')
	return ctx.body(body)
})
