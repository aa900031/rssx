import type { Env } from '../../../http'
import { vValidator } from '@hono/valibot-validator'
import * as cheerio from 'cheerio'
import { Hono } from 'hono'
import * as v from 'valibot'
import { fetchProductList, fetchSellerProductIdList, fetchStoreInfo, MAIN_URL } from '../api/client'

export default new Hono<Env>()
	.get(
		'/ruten/store/:id',
		vValidator('param', v.object({
			id: v.string(),
		})),
		async (ctx) => {
			const { id } = ctx.req.param()
			const info = await fetchStoreInfo(id)
			const productIds = await fetchSellerProductIdList({
				storeId: info.data.user_id,
				sort: 'new/dc',
				offset: 1,
			})
			const products = await fetchProductList(productIds.Rows.map(item => item.Id))

			ctx.set('rss', {
				data: {
					title: info.data.store_name,
					description: info.data.board_intro,
					language: 'zh-Hant-TW',
					link: new URL(`/store/${id}`, MAIN_URL).href,
					items: products.data.map((product) => {
						const link = createProductUrl(product.id)
						const $ = cheerio.load('')

						$.root().append(`
							<table>
								<tr>
									<td>
										價格
									</td>
									<td>
										${product.goods_price}
									</td>
								</tr>
								<tr>
									<td>
										庫存
									</td>
									<td>
										${product.num}
									</td>
								</tr>
							</table>
						`)
						$.root().append('<br>')

						product.images.url.forEach((image) => {
							$.root().append(`<img src="${image}">`)
							$.root().append('<br>')
						})

						const description = ($.root().html() ?? '')
							.replace('<html><head></head><body></body></html>', '')

						return {
							title: product.name,
							link: link.href,
							guid: link.href,
							description,
							pubDate: new Date((+product.post_time) * 1000),
						}
					}),
				},
			})
		},
	)

function createProductUrl(productId: string) {
	return new URL(`/item/show?${productId}`, MAIN_URL)
}
