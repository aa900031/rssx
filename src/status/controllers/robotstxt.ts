import { Hono } from 'hono'

export const robotstxt = new Hono()
	.get('/robots.txt', (ctx) => {
		ctx.status(404)
		return ctx.text('')
	})
