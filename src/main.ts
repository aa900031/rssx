import toMilliseconds from '@sindresorhus/to-milliseconds'
import { asyncExitHook } from 'exit-hook'
import { Hono } from 'hono'
import pWaitFor from 'p-wait-for'
import { Config } from './config/config'
import Rss from './rss/http'
import Status from './status/http'

const app = new Hono()
	.route('/', Status)
	.route('/', Rss)

const server = Bun.serve({
	port: Config.api.port,
	fetch: app.fetch,
})

console.log(`Listening on http://localhost:${server.port}...`)

asyncExitHook(async () => {
	console.log('exit hook: api server')
	server.stop()
	await Promise.all([
		pWaitFor(() => server.pendingRequests === 0),
		pWaitFor(() => server.pendingWebSockets === 0),
	])
}, {
	wait: toMilliseconds({
		seconds: 3,
	}),
})
