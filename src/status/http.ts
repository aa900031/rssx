import { Hono } from 'hono'
import { healthz } from './controllers/healthz'
import { robotstxt } from './controllers/robotstxt'

export default new Hono()
	.route('/', healthz)
	.route('/', robotstxt)
