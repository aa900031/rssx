import { fileURLToPath, URL } from 'node:url'
import { Eta } from 'eta'

export interface RssData {
	title: string
	link: string
	language?: string
	description?: string
	lastBuildDate?: Date
	ttl?: number
	items?: {
		title: string
		link: string
		description: string
		guid?: string
		pubDate?: Date | null
	}[]
}

export interface RenderRssProps {
	data: RssData
}

const eta = new Eta({
	views: fileURLToPath(new URL('./templates', import.meta.url)),
	autoTrim: 'nl',
	rmWhitespace: true,
})

export async function renderRss(
	{
		data,
	}: RenderRssProps,
): Promise<string> {
	const content = await eta.renderAsync('./rss', resolveData(data))
	return content ?? ''
}

function resolveData(data: RssData) {
	return {
		title: data.title,
		link: data.link,
		language: data.language ?? 'en',
		description: data.description ?? data.title,
		lastBuildDate: data.lastBuildDate?.toUTCString() ?? new Date().toUTCString(),
		ttl: data.ttl ?? 60,
		items: data.items?.map(item => ({
			guid: item.guid ?? item.link,
			title: item.title,
			link: item.link,
			description: item.description,
			pubDate: item.pubDate?.toUTCString(),
		})) ?? [],
	}
}
