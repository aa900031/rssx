import { $fetch } from '../../../../cralwer/fetch'

export const MAIN_URL = 'https://www.ruten.com.tw'

export const API_URL = 'https://rapi.ruten.com.tw'

export const API2_URL = 'https://rtapi.ruten.com.tw'

export function fetchStoreInfo(storeAccount: string) {
	const url = new URL(
		`/api/users/v1/index.php/${storeAccount}/storeinfo`,
		API_URL,
	)

	return $fetch<{
		status: string
		data: {
			store_name: string
			board_intro: string
			store_bg_img: string
			board_img: string
			board_is_new: boolean
			board_is_active: boolean
			credit_rate: number
			im_response_time_hours: number
			items_cnt: number
			credit_cnt: number
			foreign_credit: number
			deliver_days: string
			phone_certify: boolean
			email_certify: boolean
			pp_certify: boolean
			pp_crd_certify: boolean
			pi_level: string
			corp_status: boolean
			oversea_user: boolean
			safepc: boolean
			user_img: string
			store_suspend_info: null
			platform: string
			last_activity_time: number
			user_id: string
		}
	}>(url)
}

export async function fetchSellerProductIdList({
	storeId,
	sort,
	offset = 1,
	limit = 30,
}: {
	storeId: string
	sort: string
	offset?: number
	limit?: number
}): Promise<{
		TotalRows: number
		LimitedTotalRows: number
		Rows: {
			Id: string
		}[]
	}> {
	const url = new URL(
		`/api/search/v3/index.php/core/seller/${storeId}/prod`,
		API2_URL,
	)
	url.searchParams.set('sort', sort)
	url.searchParams.set('limit', `${limit}`)
	url.searchParams.set('offset', `${offset}`)
	url.searchParams.set('_', `${Date.now()}`)
	url.searchParams.set('_callback', 'axiosJsonpCallback1')

	const script = await $fetch(url, { responseType: 'text' })

	const jsonText = script
		.replace('try{axiosJsonpCallback1(', '')
		.replace(');}catch(e){if(window.console){console.log(e);}}', '')

	return JSON.parse(jsonText)
}

export function fetchProductList(ids: string[]) {
	const url = new URL(
		'/api/items/v2/list',
		API_URL,
	)
	url.searchParams.set('gno', ids.join(','))
	url.searchParams.set('level', 'simple')

	return $fetch<{
		status: string
		data: {
			id: string
			class: string
			name: string
			num: number
			images: {
				filename: string
				url: string[]
				m_url: string[]
			}
			currency: string
			goods_price: string
			goods_ori_price: string
			goods_price_range: {
				min: number
				max: number
				ori_min: number
				ori_max: number
			}
			watch_num: number
			sold_num: number
			buyer_limit_num: number
			stock_status: number
			youtube_link: string
			ncc_check_code: string
			goods_no: string
			available: boolean
			post_time: string
			spec_type: string
			deliver_way: {
				SEVEN_COD: number
				SEVEN: number
				HOUSE: number
				SELF: number
			}
			mode: string
			ship: string
			platform: string
			ctrl_rowid: string
			user: string
			user_credit: number
			pay_way: string[]
			title_style: string
			selling_g_now_price: number
			item_remaining_time: number
			translation_type: null
			translated_name: null
			min_estimated_delivery_date: null
			max_estimated_delivery_date: null
			update_time: string
		}[]
	}>(url)
}
