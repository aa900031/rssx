import { ofetch } from 'ofetch'
import { Config } from '../config/config'

export const $fetch = ofetch.create({
	retryStatusCodes: [400, 408, 409, 425, 429, 500, 502, 503, 504],
	retry: 5,
	retryDelay: 1000,
	headers: {
		'User-Agent': Config.crawler.userAgent,
	},
})
