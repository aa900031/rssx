import type { Env } from '../../../http'
import type { RssData } from '../../../render'
import { vValidator } from '@hono/valibot-validator'
import { Hono } from 'hono'
import * as v from 'valibot'
import { fetchPage, fetchPostDetail } from '../api/client'

export default new Hono<Env>()
	.get(
		'/facebook/page/:id',
		vValidator('param', v.object({
			id: v.string(),
		})),
		async (ctx) => {
			const { id } = ctx.req.param()
			const pageData = await fetchPage({ pageId: id })

			const { posts, ...otherPageData } = pageData
			const items: NonNullable<RssData['items']> = []
			for (let index = 0; index < posts.length; index++) {
				const post = posts[index]

				const postDetail = await fetchPostDetail(post)
				if (postDetail) {
					items[index] = postDetail
				}
			}

			ctx.set('rss', {
				data: {
					...otherPageData,
					items,
				},
			})
		},
	)
