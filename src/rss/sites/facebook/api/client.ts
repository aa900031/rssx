import { URL } from 'node:url'
import * as cheerio from 'cheerio'
import { $fetch } from '../../../../cralwer/fetch'

const BASIC_URL = 'https://mbasic.facebook.com'

const PRODUCTION_URL = 'https://www.facebook.com'

const WRAP_REDIRECT_URL = 'https://lm.facebook.com/l.php'

enum UrlType {
	Production,
	Basic,
}

export async function fetchPhotoSet(storyId: string, paths: string[]) {
	const paramsMap = paths.reduce<Record<string, NonNullable<ReturnType<typeof toPhotoPhpParams>>>>((obj, item) => {
		const result = toPhotoPhpParams(storyId, item)
		if (result)
			obj[result.photoId] = result
		return obj
	}, {})
	const photoIds = new Set(Object.keys(paramsMap))
	const photos: ReturnType<typeof parseStoryPhoto>[] = []

	for (const photoId of photoIds) {
		const params = paramsMap[photoId]
		if (!params)
			continue
		const url = createPhotoPhpUrl(UrlType.Basic, params)
		const html = await $fetch(url, { responseType: 'text' })
		const data = parseStoryPhoto(html)
		photos.push(data)

		if (/^pcb\./.test(params.setId) && data.nextLink) {
			const nextParams = toPhotoPhpParams(storyId, data.nextLink)
			if (nextParams) {
				paramsMap[nextParams.photoId] = nextParams
				photoIds.add(nextParams.photoId)
			}
		}
	}

	return photos
}

export async function fetchPostDetail(data: ReturnType<typeof parsePage>['posts'][0]) {
	if (isStoryPath(data.path)) {
		const storyParams = getStoryPathParams(data.path)
		const storyHtml = await $fetch(createStoryUrl(UrlType.Basic, storyParams!), { responseType: 'text' })

		const storyData = parseStory(storyHtml)
		const storyPhotos = await fetchPhotoSet(storyParams!.articleId, storyData.photos)

		const isSingleImageStory = storyPhotos.length === 1
		const isEmptyImageList = storyPhotos.length === 0

		let desc = ''

		desc += storyPhotos
			.map(image => `<img src="${image.image}"><br>${image.description}`)
			.join('<br>')

		if (!isSingleImageStory) {
			if (!isEmptyImageList) {
				desc += '<br>'
			}
			desc += storyData.description
		}

		return {
			title: storyData.title,
			link: createStoryUrl(UrlType.Production, storyParams!).href,
			description: desc,
			pubDate: data.pubDate,
		}
	}
	else if (isStoryPhotoPath(data.path)) {
		const photoParams = getStoryPhotoPathParams(data.path)
		const photoHtml = await $fetch(createStoryPhotoUrl(UrlType.Basic, photoParams!), { responseType: 'text' })
		const photoData = parseStoryPhoto(photoHtml)

		return {
			title: photoData.title,
			link: createStoryPhotoUrl(UrlType.Production, photoParams!).href,
			description: `<img src="${photoData.image}"><br>${photoData.description}`,
			pubDate: data.pubDate,
		}
	}
}

export async function fetchPage({
	pageId,
}: {
	pageId: string
}) {
	const url = createPageUrl(UrlType.Basic, { pageId })
	const pageHtml = await $fetch(url, { responseType: 'text' })
	const pageData = parsePage(pageHtml)

	return {
		title: pageData.title,
		description: pageData.description,
		language: pageData.language,
		link: createPageUrl(UrlType.Production, { pageId }).href,
		posts: pageData.posts,
	}
}

function getBaseUrl(type: UrlType) {
	switch (type) {
		case UrlType.Basic:
			return BASIC_URL
		case UrlType.Production:
			return PRODUCTION_URL
	}
}
function createPageUrl(type: UrlType, params: {
	pageId: string
}) {
	const url = new URL(
		`/${params.pageId}`,
		getBaseUrl(type),
	)

	url.searchParams.set('v', 'timeline')
	return url
}

function parsePage(body: string) {
	const $ = cheerio.load(body)
	const $articles = $('#tlFeed article')

	const articles = $articles.toArray()
		.map((article) => {
			const $article = $(article)
			const $link = $article.find('footer a:nth-child(1)').eq(0)
			const data = JSON.parse($article.attr('data-ft') ?? '{}')
			const publishTime = data?.page_insights?.[data.page_id]?.post_context?.publish_time ?? null
			const path = $link.attr('href')
			if (!path)
				// eslint-disable-next-line array-callback-return
				return

			return {
				path,
				pubDate: typeof publishTime === 'number'
					? new Date(publishTime * 1000)
					: null,
			}
		})
		.filter(Boolean) as { path: string, pubDate: Date | null }[]

	return {
		title: $('#m-timeline-cover-section h1 span').text(),
		description: $('meta[property="og:description"]').attr('content') ?? '',
		posts: articles,
		language: $('html').attr('lang'),
	}
}

function isStoryPhotoPath(url: string): boolean {
	return /^\/.+\/photos\//.test(url)
}

function getStoryPhotoPathParams(url: string) {
	// eslint-disable-next-line regexp/no-misleading-capturing-group
	const groups = url.match(/^\/(.+)\/photos\/(.+)\/(.+)\//)
	if (!groups)
		return null

	const [_, pageId, setId, photoId] = groups

	return {
		pageId,
		setId,
		photoId,
	}
}

function createStoryPhotoUrl(type: UrlType, params: {
	pageId: string
	setId: string
	photoId: string
}) {
	return new URL(
		`/${params.pageId}/photos/${params.setId}/${params.photoId}`,
		getBaseUrl(type),
	)
}

function isPhotoPhpPath(path: string): boolean {
	try {
		const url = new URL(path, 'http://example.com')
		return url.pathname === '/photo.php'
			&& url.searchParams.has('fbid')
			&& url.searchParams.has('id')
			&& url.searchParams.has('set')
	}
	catch {
		return false
	}
}

function getPhotoPhpPathParams(path: string) {
	const url = new URL(path, 'http://example.com')
	return {
		articleId: url.searchParams.get('id')!,
		photoId: url.searchParams.get('fbid')!,
		setId: url.searchParams.get('set')!,
	}
}

function createPhotoPhpUrl(type: UrlType, params: {
	articleId: string
	photoId: string
	setId: string
}) {
	const url = new URL(
		`/photo.php`,
		getBaseUrl(type),
	)

	url.searchParams.set('fbid', params.photoId)
	url.searchParams.set('id', params.articleId)
	url.searchParams.set('set', params.setId)

	return url
}

function parseStoryPhoto(body: string) {
	const $ = cheerio.load(body)

	const title = $('meta[property="og:description"][content]').attr('content')?.toString() ?? ''
	const description = $('#MPhotoContent div.msg > div').html()
	const image = $('#MPhotoContent div.desc.attachment > span > div > span > a[target=_blank].sec').attr('href')
	const prevLink = $(`table a[href^="/photo.php"]`).first().attr('href')
	const nextLink = $(`table a[href^="/photo.php"]`).last().attr('href')

	return {
		title,
		description,
		image,
		prevLink,
		nextLink,
	}
}

function getStoryFbId(url: URL) {
	const storyFbId = url.searchParams.get('story_fbid')
	if (!storyFbId)
		return null

	if (/^\d+$/.test(storyFbId)) {
		return storyFbId
	}

	const [_, mfStoryKey] = /mf_story_key\.(\d+):/.exec(url.searchParams.get('_ft_') ?? '') ?? []

	if (storyFbId.startsWith('pfbid')) {
		return mfStoryKey ?? null
	}

	return null
}

function isStoryPath(path: string): boolean {
	try {
		const url = new URL(path, 'http://example.com')
		if (url.pathname !== '/story.php')
			return false
		const id = url.searchParams.get('id')
		const stroyId = getStoryFbId(url)
		return !!stroyId && !!id
	}
	catch {
		return false
	}
}

function getStoryPathParams(path: string) {
	const url = new URL(path, 'http://example.com')
	const storyFbId = getStoryFbId(url)

	return {
		storyId: storyFbId!,
		articleId: url.searchParams.get('id')!,
	}
}

function createStoryUrl(type: UrlType, params: {
	storyId: string
	articleId: string
}) {
	const url = new URL('/story.php', getBaseUrl(type))
	url.searchParams.set('story_fbid', params.storyId)
	url.searchParams.set('id', params.articleId)
	return url
}

function parseStory(body: string) {
	const $ = cheerio.load(body)
	const $story = $('#m_story_permalink_view div[data-ft]').eq(0)
	const $content = $story.find(`div[data-ft="{\"tn\":\"*s\"}"]`).eq(0)
	const $attach = $story.find(`div[data-ft="{\"tn\":\"H\"}"]`).eq(0)

	const getTitle = () => {
		const text = $('title').text()
		const regex = /^.+\s-\s([\s\S]+)\s\|\sFacebook$/
		const groups = regex.exec(text)
		if (!groups)
			return text
		return groups[1]
	}

	const getContent = () => {
		$content.find(`a[href^="${WRAP_REDIRECT_URL}"]`).each((_, ele) => {
			const link = $(ele)
			const originalLink = new URL(link.attr('href') ?? '').searchParams.get('u')
			if (originalLink) {
				link.attr('href', decodeURIComponent(originalLink))
			}
		})

		return $content.html()
	}

	const getPosts = () => {
		const attachLinkList = $attach
			.find('a')
			.toArray()
			.map(a => $(a).attr('href'))
			.filter(Boolean) as string[]
		const isAttachAreImageSet = attachLinkList
			.filter(link => isStoryPhotoPath(link))
			.length === attachLinkList.length

		return isAttachAreImageSet ? attachLinkList : []
	}

	return {
		title: getTitle(),
		description: getContent(),
		photos: getPosts(),
	}
}

function toPhotoPhpParams(articleId: string, path: string) {
	if (isStoryPhotoPath(path)) {
		const params = getStoryPhotoPathParams(path)
		return {
			articleId,
			setId: params!.setId,
			photoId: params!.photoId,
		}
	}
	else if (isPhotoPhpPath(path)) {
		const params = getPhotoPhpPathParams(path)
		return params
	}
}
