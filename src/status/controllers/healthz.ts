import { Hono } from 'hono'

export const healthz = new Hono()
	.get('/healthz', (ctx) => {
		ctx.header('Cache-Control', 'no-cache')
		return ctx.text('ok')
	})
